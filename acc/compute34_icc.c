#include "compute.h"
#include "omp.h"
#include <sys/time.h>
#include <time.h>
#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>

#ifndef M_SQRT2
# define M_SQRT2 1.41421356237309504880
#endif

#define likely(x)      __builtin_expect(!!(x), 1)
#define unlikely(x)    __builtin_expect(!!(x), 0)

static void *xmalloc(size_t size) {
    void *ret = malloc(size);
    if (ret == NULL)
        exit(ENOMEM);
    return ret;
}

#define update_cell(N, M, grid, cond, i, j, src, dst)                    \
    ({                                                                   \
     double icond = (1.0 - (*cond)[i][j]);                               \
     (*grid)[i][j][dst] =                                                \
        /* Previous value * its conductivity */                          \
        (*cond)[i][j] * (*grid)[i][j][src]                               \
        /* Direct neighbors */                                           \
        + ((*grid)[i + 1][j][src] + (*grid)[i - 1][j][src]               \
            + (*grid)[i][j + 1][src] + (*grid)[i][j - 1][src]) * icond   \
            * nbr_coeff                                                  \
        /* Diagonal neighbors */                                         \
        + ((*grid)[i + 1][j + 1][src] + (*grid)[i + 1][j - 1][src]       \
            + (*grid)[i - 1][j + 1][src] + (*grid)[i - 1][j - 1][src])   \
            * icond * dnbr_coeff;                                        \
     /* Return current difference */                                     \
     fabs((*grid)[i][j][0] - (*grid)[i][j][1]);                          \
     })

static inline __attribute__((target(mic))) double heat_iterate(size_t N, size_t M,
        double (*restrict grid)[N][M][2], double (*restrict cond)[N][M],
        size_t idx) {
    size_t src = idx;
    size_t dst = 1 - idx;

    /* Border case 'wrap': copy the opposite column */
    double curdiff;
    double maxdiff = -INFINITY;

    /* Update the grid */
    {
        static const double __attribute__((target(mic))) nbr_coeff = 0.25f * M_SQRT2 / (M_SQRT2 + 1.0f);
        static const double __attribute__((target(mic))) dnbr_coeff = 0.25f * 1.0f / (M_SQRT2 + 1.0f);

        # pragma omp parallel for shared(N, M, src, grid)
        for (size_t i = 0; i < N; ++i) {
            (*grid)[i][0][src] = (*grid)[i][M - 2][src];
            (*grid)[i][M - 1][src] = (*grid)[i][1][src];
        }

        # pragma omp parallel for private(curdiff) \
            shared(N, M, grid, cond, src, dst) \
            reduction(max: maxdiff) \
            collapse(2)
        for (size_t i = 1; i < N - 1; ++i) {
            for (size_t j = 1; j < M - 1; ++j) {
                curdiff = update_cell(N, M, grid, cond, i, j, src, dst);
                if (maxdiff < curdiff)
                    maxdiff = curdiff;
            }
        }
    }

    return maxdiff;
}

static void heat_report_update(size_t N, size_t M,
        double (*restrict grid)[N][M][2], struct results *r,
        struct timeval start, struct timeval end, int idx) {
    int src = idx;
    double s = 0.0;

    /* Compute time */
    r->time = ((double) end.tv_sec + 1.0e-6 * end.tv_usec)
        - ((double) start.tv_sec + 1.0e-6 * start.tv_usec);

    r->tmin = INFINITY;
    r->tmax = -INFINITY;

    /* Min/Max */
    size_t i, j;
    double t;
    double tmin = r->tmin;
    double tmax = r->tmax;
    double diff = -INFINITY;
    r->maxdiff = -INFINITY;

    for (size_t i = 1; i < N - 1; ++i) {
        for (size_t j = 1; j < M - 1; ++j) {
            double t = (*grid)[i][j][src];
            s += t;
            if (tmin > t)
                tmin = t;
            if (tmax < t)
                tmax = t;
            diff = fabs((*grid)[i][j][0] - (*grid)[i][j][1]);
            if (diff > r->maxdiff)
                r->maxdiff = diff;
        }
    }

    /* Assign min, max */
    r->tmin = tmin;
    r->tmax = tmax;

    /* Average Temperature */
    r->tavg = s / ((N - 2) * (M - 2));
}

void do_compute(const struct parameters *p, struct results *r) {
    size_t i;
    /* Add 1 column right and left to handle wrap */
    size_t M = p->M + 2;
    /* Add 1 row top and bottom to handle border (no wrap) */
    size_t N = p->N + 2;

    /* Map the 1D conductivity array to a 2D representation */
    double (*cond)[N][M] = xmalloc(N * M * sizeof(double));

    /* Create an array with the old and the new value of the grid */
    double (*grid)[N][M][2] = xmalloc(N * M * 2 * sizeof(double));

    /* Copy temperature and conductivity initial value */
    for (i = 0; i < p->M * p->N; ++i) {
        (*cond)[i / p->M + 1][i % p->M + 1] = p->conductivity[i];
        (*grid)[i / p->M + 1][i % p->M + 1][0] = p->tinit[i];
    }

    /* Border case: values for the borders (copy the nearest row) */
    for (size_t i = 1; i < M - 1; ++i) {
        (*grid)[0][i][0] = (*grid)[0][i][1] = (*grid)[1][i][0];
        (*grid)[N - 1][i][0] = (*grid)[N - 1][i][1] = (*grid)[N - 2][i][0];
    }

    struct timeval start = { 0, 0 }, end = { 0, 0 };
    *r = (struct results ) {
        .niter = 0,
            .tmin = INFINITY,
            .tmax = -INFINITY,
            .maxdiff = -INFINITY,
            .tavg = 0,
            .time = 0,
    };

    size_t maxiter = p->maxiter;
    double maxdiff = 0.0f;
    int period = p->period;

    #pragma offload target(mic) in (cond[0:1][0:N][0:M],       \
                                        maxiter, N, M)         \
                                inout (maxdiff, grid[0:1][0:N][0:M][0:2])
    {
        gettimeofday(&start, NULL);
        for (i = 0; i < maxiter; ++i) {
            maxdiff = heat_iterate(N, M, grid, cond, i % 2);
        }
        gettimeofday(&end, NULL);
    }

    heat_report_update(N, M, grid, r, start, end, i % 2);
    r->niter = maxiter;
    r->maxdiff = maxdiff;
    free(cond);
    free(grid);
}
