#include "compute.h"
#include "omp.h"
#include <sys/time.h>
#include <time.h>
#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>

#define likely(x)      __builtin_expect(!!(x), 1)
#define unlikely(x)    __builtin_expect(!!(x), 0)

static const double nbr_coeff = 0.25f * M_SQRT2 / (M_SQRT2 + 1.0f);
static const double dnbr_coeff = 0.25f * 1.0f / (M_SQRT2 + 1.0f);
static size_t ratio = 1000;
static size_t cpu_N = 1;

static void *xmalloc(size_t size) {
	void *ret = malloc(size);
	if (ret == NULL)
		exit(ENOMEM);
	return ret;
}

static void dump_grid(size_t N, size_t M, double (*restrict grid)[N][M][2], int idx)
{
    for (int i = 0; i < 6; ++i) {
        for (int j = 0; j < 6; ++j)
            printf("%f ", (*grid)[i][j][idx]);
        printf("\n");
    }
}


#define update_cell(N, M, grid, cond, i, j, src, dst, r)                 \
    ({                                                                   \
     double icond = (1.0 - (*cond)[i][j]);                               \
     (*grid)[i][j][dst] =                                                \
        /* Previous value * its conductivity */                          \
        (*cond)[i][j] * (*grid)[i][j][src]                               \
        /* Direct neighbors */                                           \
        + ((*grid)[i + 1][j][src] + (*grid)[i - 1][j][src]               \
            + (*grid)[i][j + 1][src] + (*grid)[i][j - 1][src]) * icond   \
            * nbr_coeff                                                  \
        /* Diagonal neighbors */                                         \
        + ((*grid)[i + 1][j + 1][src] + (*grid)[i + 1][j - 1][src]       \
            + (*grid)[i - 1][j + 1][src] + (*grid)[i - 1][j - 1][src])   \
            * icond * dnbr_coeff;                                        \
     /* Return current difference */                                     \
     fabs((*grid)[i][j][0] - (*grid)[i][j][1]);                          \
     })

static inline void heat_iterate(size_t N, size_t M,
        double (*restrict grid)[N][M][2], double (*restrict cond)[N][M],
        struct results *restrict r, size_t idx) {
    size_t src = idx;
    size_t dst = 1 - idx;

    double maxdiff1 = -INFINITY;
    double maxdiff2 = -INFINITY;

    size_t size = (N - 2) * (M - 2);
    size_t cpu_size = (size_t)(size / (ratio + 1));

    cpu_N = cpu_size / N;
    if (cpu_N == 0)
        cpu_N = 1;

#pragma omp parallel shared(N, M, grid, cond, src, dst, r, maxdiff1, maxdiff2)
{
#pragma omp single nowait
{
   /* This task handle the CPU */
   #pragma omp task untied if (cpu_N > 1)
   {
       /* Border case 'wrap': copy the opposite column */
       # pragma omp parallel for
       for (size_t i = 0; i < cpu_N+1; ++i) {
           (*grid)[i][0][src] = (*grid)[i][M - 2][src];
           (*grid)[i][M - 1][src] = (*grid)[i][1][src];
       }

       /* The pragma should use a collapse(2), but the compiler miserably
        * throw an error: PGC-S-0043-Redefinition of symbol, __FUNCTION__ */
       # pragma omp parallel for reduction(max: maxdiff1) collapse(1)
       for (size_t i = 1; i < cpu_N; ++i) {
           # pragma omp parallel for reduction(max: maxdiff1)
           for (size_t j = 1; j < M - 1; ++j) {
               /* The compiler does not support VLA as a parameter in
                * functions executed on the device.
                * Use GCC's Statement expressions to return a value
                * with a macro:
                *   https://gcc.gnu.org/onlinedocs/gcc/Statement-Exprs.html
                */
               double curdiff = update_cell(N, M, grid, cond, i, j, src, dst, r);
               if (maxdiff1 < curdiff)
                   maxdiff1 = curdiff;
               /* DO NOT REMOVE this line or the 'volatile'.
                * The fucking compiler has been hacked here.
                * Without this line (*grid)[i][j][dst] is not updated.
                * PGCC owes me 8h of my life for this line.
                */
               volatile double lol = 42;
           }
       }
   }

    /* GPU part */
    #pragma omp task untied
    {
        #pragma acc region present(grid, cond, nbr_coeff, dnbr_coeff) \
                           copyin (dst, src, cpu_N, N, M) \
                           copy(maxdiff2)
        {
            /* Border case 'wrap': copy the opposite column */
            #pragma acc loop independent
            for (size_t i = cpu_N-1; i < N; ++i) {
                (*grid)[i][0][src] = (*grid)[i][M - 2][src];
                (*grid)[i][M - 1][src] = (*grid)[i][1][src];
            }

            #pragma acc loop independent gang
            for (size_t i = cpu_N; i < N - 1; ++i) {
                #pragma acc loop independent vector(16) unroll(4) \
                    reduction(max: maxdiff2)
                for (size_t j = 1; j < M - 1; ++j) {
                    double curdiff = update_cell(N, M, grid, cond, i, j, src, dst, r);
                    if (maxdiff2 < curdiff)
                        maxdiff2 = curdiff;
                }
            }
        }
    }
    /* Wait for the CPU *and* the GPU to finish their jobs */
    #pragma omp taskwait
    #pragma acc update device (grid[0:1][cpu_N -1 : 1][0:M][0:2])
    #pragma acc update host   (grid[0:1][cpu_N    : 1][0:M][0:2])

    /* Debug: copy back the whole grid from the device to display it */
    #if 0
    #pragma acc update host   (grid[0:1][cpu_N    : N-cpu_N][0:M][0:2])
    dump_grid(N, M, grid, dst);
    #endif
}
}

    r->maxdiff = cpu_N <= 1 ? maxdiff2 : (maxdiff1 > maxdiff2 ? maxdiff1 : maxdiff2);
}

static void heat_report_update(size_t N, size_t M,
        double (*restrict grid)[N][M][2], struct results *r,
        struct timeval start, int idx) {
    int src = idx;
    struct timeval end;
    double s = 0.0;

    /* Compute time */
    gettimeofday(&end, NULL);
    r->time = ((double) end.tv_sec + 1.0e-6 * end.tv_usec)
        - ((double) start.tv_sec + 1.0e-6 * start.tv_usec);

    r->tmin = INFINITY;
    r->tmax = -INFINITY;

    /* Min/Max */
    size_t i, j;
    double t;
    double tmin = r->tmin;
    double tmax = r->tmax;
    double diff = -INFINITY;
    r->maxdiff = -INFINITY;

    //#pragma acc region present(grid)
    {
        //#pragma acc loop independent gang
        for (size_t i = 1; i < N - 1; ++i) {
            //#pragma acc loop independent vector \
            //    reduction(+: s)                 \
            //    reduction(min: tmin)            \
            //    reduction(max: tmax)
            for (size_t j = 1; j < M - 1; ++j) {
                double t = (*grid)[i][j][src];
                s += t;
                if (tmin > t)
                    tmin = t;
                if (tmax < t)
                    tmax = t;
                diff = fabs((*grid)[i][j][0] - (*grid)[i][j][1]);
                if (diff > r->maxdiff)
                    r->maxdiff = diff;
            }
        }
    }

    /* Assign min, max */
    r->tmin = tmin;
    r->tmax = tmax;

    /* Average Temperature */
    r->tavg = s / ((N - 2) * (M - 2));
}

void do_compute(struct parameters *p, struct results *r) {
	size_t i;
	/* Add 1 column right and left to handle wrap */
	size_t M = p->M + 2;
	/* Add 1 row top and bottom to handle border (no wrap) */
	size_t N = p->N + 2;

	/* Map the 1D conductivity array to a 2D representation */
	double (*cond)[N][M] = xmalloc(N * M * sizeof(double));

	/* Create an array with the old and the new value of the grid */
	double (*grid)[N][M][2] = xmalloc(N * M * 2 * sizeof(double));

	/* Copy temperature and conductivity initial value */
	for (i = 0; i < p->M * p->N; ++i) {
		(*cond)[i / p->M + 1][i % p->M + 1] = p->conductivity[i];
		(*grid)[i / p->M + 1][i % p->M + 1][0] = p->tinit[i];
		(*grid)[i / p->M + 1][i % p->M + 1][1] = 0.42f;
	}

	/* Border case: values for the borders (copy the nearest row) */
	for (size_t i = 1; i < M - 1; ++i) {
		(*grid)[0][i][0] = (*grid)[0][i][1] = (*grid)[1][i][0];
		(*grid)[N - 1][i][0] = (*grid)[N - 1][i][1] = (*grid)[N - 2][i][0];
	}

	struct timeval start = { 0, 0 };
	*r = (struct results ) {
		.niter = 0,
		.tmin = INFINITY,
		.tmax = -INFINITY,
		.maxdiff = -INFINITY,
		.tavg = 0,
		.time = 0,
	};

        char *env_ratio = getenv("ACC_OMP_RATIO");
        if (env_ratio)
            ratio = atoi(env_ratio);

        printf("using CPU/GPU ratio = %zu\n", ratio);

	gettimeofday(&start, NULL);
/* Set the whole gride on the device */
#pragma acc data copyin  (grid[:1][:N][:M][:2], \
                          cond[:1][:N][:M],     \
                          nbr_coeff,            \
                          dnbr_coeff)
{
	for (i = 0; i < p->maxiter; ++i) {
		heat_iterate(N, M, grid, cond, r, i % 2);
		if (unlikely(i % p->period == p->period - 1)) {
			heat_report_update(N, M, grid, r, start, (i + 1) % 2);
			if (r->maxdiff < p->threshold && isfinite(r->maxdiff)) {
				// This increment is here so that the assignment
				// below the for loop reflects the correct number
				// of iterations processed
				++i;
				break;
			}
			r->niter = i + 1;
			report_results(p, r);
			//grid_to_pgm(N, M, grid, i % 2, r, p);
		}
	}
	r->niter = i;
#pragma acc update host   (grid[0:1][cpu_N    : N-cpu_N][0:M][0:2])
	heat_report_update(N, M, grid, r, start, i % 2);
}
	//grid_to_pgm(N, M, grid, i % 2, r, p);
	free(cond);
	free(grid);
}
