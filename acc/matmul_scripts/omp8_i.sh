export OMP_NUM_THREADS=8;../matmul_omp 1000 1
export OMP_NUM_THREADS=8;../matmul_omp 1000 5
export OMP_NUM_THREADS=8;../matmul_omp 1000 10
export OMP_NUM_THREADS=8;../matmul_omp 1000 20
export OMP_NUM_THREADS=8;../matmul_omp 1000 30
export OMP_NUM_THREADS=8;../matmul_omp 1000 50
export OMP_NUM_THREADS=8;../matmul_omp 1000 100
export OMP_NUM_THREADS=8;../matmul_omp 1000 200