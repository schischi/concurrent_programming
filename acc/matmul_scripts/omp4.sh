export OMP_NUM_THREADS=4;../matmul_omp 100 1
export OMP_NUM_THREADS=4;../matmul_omp 300 1
export OMP_NUM_THREADS=4;../matmul_omp 500 1
export OMP_NUM_THREADS=4;../matmul_omp 1000 1
export OMP_NUM_THREADS=4;../matmul_omp 2000 1
export OMP_NUM_THREADS=4;../matmul_omp 3000 1
export OMP_NUM_THREADS=4;../matmul_omp 4000 1
export OMP_NUM_THREADS=4;../matmul_omp 5000 1