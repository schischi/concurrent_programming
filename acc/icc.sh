#! /bin/sh

icc -O2 -openmp -Wall -std=c99 -DHAVE_CONFIG_H -I. -I../src/ ./../src/*.c compute34_icc.c -o heat_mic

icc -O2 -openmp -mmic -Wall -std=c99 -DHAVE_CONFIG_H -I. -I../src/ ./../src/fail.c ./../src/img.c ./../src/input.c ./../src/main.c ./../src/output.c compute34_mic.c -o heat_mic_native
