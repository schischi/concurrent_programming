module compute {
use BlockDist;
use common;
use Time;
use main; //Gp

const sqrt2 = 1.4142135623730951;
const nbr_coeff  = 0.25 * sqrt2 / (sqrt2 + 1.0);
const dnbr_coeff = 0.25 * 1.0   / (sqrt2 + 1.0);

proc heat_report_update(in r : results, t : Timer, grid : [], src : int,
                        dst : int) : results
{
    r.time = t.elapsed();
    r.tmin = +INFINITY;
    r.tmax = -INFINITY;
    var sum : real = 0;
    var gridSpace = grid[src].domain.expand(-1,-1);

    for xy in gridSpace do {
        var t : real = grid[src][xy];
        sum += t;
        if (r.tmin > t) {
            r.tmin = t;
        }
        if (r.tmax < t) {
            r.tmax = t;
        }
    }
    r.tavg = sum / gridSpace.size;
    return r;
}

proc do_compute(p : params)
{
    var t : Timer;
    var r : results;

    const gridSpace = {1..p.N  , 1..p.M};
    const hgridSpace = gridSpace.expand(1,1); //grid + halo
    const lastRow       = gridSpace.exterior(+1, 0).expand(0,1);
    const firstRow      = gridSpace.exterior(-1, 0).expand(0,1);
    const firstCol      = gridSpace.exterior( 0,-1).expand(1,0);
    const lastCol       = gridSpace.exterior( 0,+1).expand(1,0);
    const secondCol     = firstCol.translate(0,1);
    const beforeLastCol = lastCol.translate(0,-1);

    var src = 1, dst = 2, cond = 3;
    var grid : [1..3] [hgridSpace] real;

    /* Initialize the grid */
    grid[cond][gridSpace] = p.tcond[gridSpace];
    grid[src][gridSpace] = p.tinit[gridSpace];
    [i in src..dst] grid[i][firstRow] = grid[src][firstRow.translate(1,0)];
    [i in src..dst] grid[i][lastRow]  = grid[src][lastRow.translate(-1,0)];

    /* Start timer. */
    writeln("Go.");
    t.start();

    for i in 1..#p.maxiter do {
        /* Border case 'wrap' */
        grid[src][firstCol] = grid[src][beforeLastCol];
        grid[src][lastCol]  = grid[src][secondCol];
        const N = p.N;
        const M = p.M;

        forall x in {1..N} do {
            for y in {1..M} do {
                grid[dst][x,y] = grid[src][x,y] * grid[cond][x,y]
                    + (grid[src][x-1,y] +
                       grid[src][x+1,y] +
                       grid[src][x,y-1] +
                       grid[src][x,y+1]) * (1.0 - grid[cond][x,y]) * nbr_coeff
                    + (grid[src][x-1,y-1] +
                       grid[src][x-1,y+1] +
                       grid[src][x+1,y-1] +
                       grid[src][x+1,y+1]) * (1.0 - grid[cond][x,y]) * dnbr_coeff;
            }
        }
        r.maxdiff = max reduce abs(grid[src][gridSpace] - grid[dst][gridSpace]);

	src <=> dst;

        if ((i - 1) % p.period == p.period - 1) {
            r.niter = i;
            r = heat_report_update(r, t, grid, src, dst);
            if (r.maxdiff < p.threshold) {
                break;
            }
            report_results(p, r);
        }
    }

    //writeln(grid[src]);

    r.niter = p.maxiter;
    r = heat_report_update(r, t, grid, src, dst);
    t.stop();

    return r;
}

}
