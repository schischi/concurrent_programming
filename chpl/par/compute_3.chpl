module compute {
use common;
use Time;

const sqrt2 = 1.4142135623730951;
const nbr_coeff  = 0.25 * sqrt2 / (sqrt2 + 1.0);
const dnbr_coeff = 0.25 * 1.0   / (sqrt2 + 1.0);

proc heat_report_update(in r : results, t : Timer, grid1 : [], grid2 : []) : results
{
    r.time = t.elapsed();
    r.tmin = +INFINITY;
    r.tmax = -INFINITY;
    var sum : real = 0;
    var gridSpace = grid1.domain.expand(-1,-1);

    for xy in gridSpace do {
        var t : real = grid1[xy];
        sum += t;
        if (r.tmin > t) {
            r.tmin = t;
        }
        if (r.tmax < t) {
            r.tmax = t;
        }
    }
    r.tavg = sum / gridSpace.size;
    return r;
}

proc do_compute(p : params)
{
    var t : Timer;
    var r : results;

    const gridSpace = {1..p.N  , 1..p.M};
    const hgridSpace = gridSpace.expand(1,1); //grid + halo
    const lastRow       = gridSpace.exterior(+1, 0).expand(0,1);
    const firstRow      = gridSpace.exterior(-1, 0).expand(0,1);
    const firstCol      = gridSpace.exterior( 0,-1).expand(1,0);
    const lastCol       = gridSpace.exterior( 0,+1).expand(1,0);
    const secondCol     = firstCol.translate(0,1);
    const beforeLastCol = lastCol.translate(0,-1);

    var grid1 : [hgridSpace] real;
    var grid2 : [hgridSpace] real;
    var cond : [gridSpace] real;

    /* Initialize the grid */
    cond[gridSpace] = p.tcond[gridSpace];
    grid1[gridSpace] = p.tinit[gridSpace];
    grid1[firstRow] = grid1[firstRow.translate(1,0)];
    grid1[lastRow]  = grid1[lastRow.translate(-1,0)];
    grid2[firstRow] = grid1[firstRow.translate(1,0)];
    grid2[lastRow]  = grid1[lastRow.translate(-1,0)];

    /* Start timer. */
    t.start();

    //serial {
        for i in 1..#p.maxiter do {
            /* Border case 'wrap' */
            grid1[firstCol] = grid1[beforeLastCol];
            grid1[lastCol]  = grid1[secondCol];

            [(x,y) in gridSpace] grid2[x,y] =
                grid1[x,y] * cond[x,y]
                + (grid1[x-1,y] +
                   grid1[x+1,y] +
                   grid1[x,y-1] +
                   grid1[x,y+1]) * (1.0 - cond[x,y]) * nbr_coeff
                + (grid1[x-1,y-1] +
                   grid1[x-1,y+1] +
                   grid1[x+1,y-1] +
                   grid1[x+1,y+1]) * (1.0 - cond[x,y]) * dnbr_coeff;

            r.maxdiff = max reduce abs(grid1[gridSpace] - grid2[gridSpace]);
	
	    grid1 <=> grid2;

            if ((i - 1) % p.period == p.period - 1) {
                r.niter = i;
                r = heat_report_update(r, t, grid1, grid2);
                if (r.maxdiff < p.threshold) {
                    break;
                }
                report_results(p, r);
            }
        }
    //}

    //writeln(grid[src]);

    r.niter = p.maxiter;
    r = heat_report_update(r, t, grid1, grid2);
    t.stop();

    return r;
}

}
