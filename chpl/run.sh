#! /bin/bash

I=1
S=1000
FILE=../acc/gradient_"$S"x"$S".pgm

export GASNET_PHYSMEM_MAX=1G
export CHPL_COMM=gasnet
export CHPL_COMM_SUBSTRATE=ibv
export GASNET_SSH_SERVERS=127.0.0.1

$1 -sGn=$S -sGm=$S -sGi=$I -sGc=$FILE -sGt=$FILE $2

#export ARGS="\"-sGn=$S -sGm=$S -sGi=$I -sGc=$FILE -sGt=$FILE \""
#rm -f FOO*
#sed -e "s/FIXME/$ARGS/" heat.job > tmp.job
#qsub tmp.job
## REF ##
#../seq/heat -n $S -m $S -i $I -c $FILE -t $FILE
