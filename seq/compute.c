#include "compute.h"
#include <sys/time.h>
#include <time.h>
#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>

#define likely(x)      __builtin_expect(!!(x), 1)
#define unlikely(x)    __builtin_expect(!!(x), 0)

static const double nbr_coeff  = 0.25f * M_SQRT2 / (M_SQRT2 + 1.0f);
static const double dnbr_coeff = 0.25f * 1.0f / (M_SQRT2 + 1.0f);

static void *xmalloc(size_t size)
{
    void *ret = malloc(size);
    if (ret == NULL)
        exit(ENOMEM);
    return ret;
}

static void grid_to_pgm(size_t N, size_t M, double (*restrict grid)[N][M][2],
                                            int idx,
                                            struct results *r,
                                            const struct parameters *p)
{
    begin_picture(r->niter, M - 2, N - 2, p->io_tmin, p->io_tmax);
    for (size_t i = 1; i < N - 1; ++i)
        for (size_t j = 1; j < M - 1; ++j)
            draw_point(j - 1, i - 1, (*grid)[i][j][idx]);
    end_picture();
}

static inline void update_cell(size_t N, size_t M,
                               double (*restrict grid)[N][M][2],
                               double (*restrict cond)[N][M],
                               size_t i, size_t j, size_t src, size_t dst,
                               struct results *restrict r)
{
    double icond = (1.0 - (*cond)[i][j]);

    (*grid)[i][j][dst] =
        /* Previous value * its conductivity */
        (*cond)[i][j] * (*grid)[i][j][src]
        /* Direct neighbors */
        + (  (*grid)[i+1][j][src]
                + (*grid)[i-1][j][src]
                + (*grid)[i][j+1][src]
                + (*grid)[i][j-1][src]) * icond * nbr_coeff
        /* Diagonal neighbors */
        + (  (*grid)[i+1][j+1][src]
                + (*grid)[i+1][j-1][src]
                + (*grid)[i-1][j+1][src]
                + (*grid)[i-1][j-1][src]) * icond * dnbr_coeff;
        /* Update maxdiff if necessary */
        double curdiff = fabs((*grid)[i][j][0] - (*grid)[i][j][1]);
        if (curdiff > r->maxdiff)
            r->maxdiff = curdiff;
}

static inline void heat_iterate(size_t N, size_t M,
                                double (*restrict grid)[N][M][2],
                                double (*restrict cond)[N][M],
                                struct results *restrict r,
                                size_t idx)
{
    size_t src = idx;
    size_t dst = 1 - idx;
    r->maxdiff = -INFINITY;

    /* Border case 'wrap': copy the opposite column */
    for (size_t i = 0; i < N; ++i) {
        (*grid)[i][0][src] = (*grid)[i][M - 2][src];
        (*grid)[i][M - 1][src] = (*grid)[i][1][src];
    }

    /* Update the grid */
    for (size_t i = 1; i < N - 1; ++i) {
        for (size_t j = 1; j < M - 1; j += 1) {
            update_cell(N, M, grid, cond, i, j, src, dst, r);
        }
    }
}

static void heat_report_update(size_t N, size_t M,
                               double (*restrict grid)[N][M][2],
                               struct results *r,
                               struct timeval start,
                               int idx)
{
    int src = idx;
    struct timeval end;
    double s = 0.0;

    /* Compute time */
    gettimeofday(&end, NULL);
    r->time = ((double)end.tv_sec + 1.0e-6 * end.tv_usec)
              - ((double)start.tv_sec + 1.0e-6 * start.tv_usec);

    r->tmin = INFINITY;
    r->tmax = -INFINITY;

    /* Min/Max */
    for (size_t i = 1; i < N - 1; ++i) {
        for (size_t j = 1; j < M - 1; ++j) {
            double t = (*grid)[i][j][src];
            s += t;
            if (r->tmin > t)
                r->tmin = t;
            if (r->tmax < t)
                r->tmax = t;
        }
    }
    /* Average Temperature */
    r->tavg = s / ((N - 2)* (M - 2));
}

void do_compute(const struct parameters *p, struct results *r)
{
    size_t i;
    /* Add 1 column right and left to handle wrap */
    const size_t M = p->M + 2;
    /* Add 1 row top and bottom to handle border (no wrap) */
    const size_t N = p->N + 2;

    /* Map the 1D conductivity array to a 2D representation */
    double (*cond)[N][M] = xmalloc(N * M * sizeof(double));

    /* Create an array with the old and the new value of the grid */
    double (*grid)[N][M][2] = xmalloc(N * M * 2 * sizeof(double));

    /* Copy temperature and conductivity initial value */
    for (i = 0; i < p->M * p->N; ++i) {
        (*cond)[i / p->M + 1][i % p->M + 1] = p->conductivity[i];
        (*grid)[i / p->M + 1][i % p->M + 1][0] = p->tinit[i];
    }

    /* Border case: values for the borders (copy the nearest row) */
    for (size_t i = 1; i < M - 1; ++i) {
        (*grid)[0][i][0] = (*grid)[0][i][1] = (*grid)[1][i][0];
        (*grid)[N - 1][i][0] = (*grid)[N - 1][i][1] = (*grid)[N - 2][i][0];
    }

    struct timeval start = { 0, 0};
    *r = (struct results) {
        .niter = 0,
        .tmin = INFINITY,
        .tmax = -INFINITY,
        .maxdiff = -INFINITY,
        .tavg = 0,
        .time = 0,
    };

    gettimeofday(&start, NULL);
    for (i = 0; i < p->maxiter; ++i) {
        heat_iterate(N, M, grid, cond, r, i % 2);
        if (unlikely(i % p->period == p->period - 1)) {
            heat_report_update(N, M, grid, r, start, (i + 1) % 2);
            if (r->maxdiff < p->threshold && isfinite(r->maxdiff)) {
                // This increment is here so that the assignment
                // below the for loop reflects the correct number
                // of iterations processed
                ++i;
                break;
            }
            r->niter = i+1;
            report_results(p, r);
            //grid_to_pgm(N, M, grid, i % 2, r, p);
        }
    }
    r->niter = i;
    heat_report_update(N, M, grid, r, start, i % 2);
    //grid_to_pgm(N, M, grid, i % 2, r, p);
    free(cond);
    free(grid);
}
