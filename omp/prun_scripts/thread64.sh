export OMP_NUM_THREADS=64;
prun -v -np 4 ../heat -i 10000 -c gradient_100x100.pgm -t plasma_100x100.pgm -m 100 -n 100 -e 0 -k 10000
export OMP_NUM_THREADS=64;
prun -v -np 4 ../heat -i 10000 -c gradient_1000x1000.pgm -t plasma_1000x1000.pgm -m 1000 -n 1000 -e 0 -k 10000
export OMP_NUM_THREADS=64;
prun -v -np 4 ../heat -i 10000 -c gradient_100x20000.pgm -t plasma_100x20000.pgm -m 100 -n 20000 -e 0 -k 10000
export OMP_NUM_THREADS=64;
prun -v -np 4 ../heat -i 10000 -c gradient_20000x100.pgm -t plasma_20000x100.pgm -m 20000 -n 100 -e 0 -k 10000
export OMP_NUM_THREADS=64;
prun -v -np 4 ../heat -i 10000 -c gradient_5000x5000.pgm -t plasma_5000x5000.pgm -m 5000 -n 5000 -e 0 -k 10000