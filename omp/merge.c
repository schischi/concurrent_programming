#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <getopt.h>
#include <stdio.h>
#include <ctype.h>
#include <sys/time.h>
#include <omp.h>

/* Ordering of the vector */
typedef enum Ordering {ASCENDING, DESCENDING, RANDOM} Order;

int debug = 0;
int chrono = 0;

static void merge(int a[], size_t start, size_t mid, size_t end, int b[])
{
    size_t i0 = start;
    size_t i1 = mid;

    for (size_t j = start; j < end; j++) {
        if (i0 < mid && (i1 >= end || a[i0] <= a[i1])) {
            b[j] = a[i0];
            i0 = i0 + 1;
        }
        else {
            b[j] = a[i1];
            i1 = i1 + 1;
        }
    }
}

static void __merge_sort(int a[], size_t start, size_t end, int b[])
{
    if(end - start < 2)
        return;

    size_t i;
    size_t mid = start + (end - start) / 2;

    #pragma omp task shared(a, b) firstprivate (start, mid, end)
    __merge_sort(a, start,  mid, b);
    #pragma omp task shared(a, b) firstprivate (start, mid, end)
    __merge_sort(a, mid, end, b);
    #pragma omp taskwait
    merge(a, start, mid, end, b);

    /* Copy array */
    #pragma omp parallel for shared (a, b) private(i)
    for(i = start; i < end; ++i)
        a[i] = b[i];
}

/* Sort vector v of l elements using mergesort */
void msort(int *v, long l)
{
    int *b = malloc(l * sizeof(int));
    #pragma omp parallel
    {
        #pragma omp single
        __merge_sort(v, 0, l, b);
    }
}

static void print_v(int *v, long l) {
    printf("\n");
    for(long i = 0; i < l; i++) {
        if(i != 0 && (i % 10 == 0)) {
            printf("\n");
        }
        printf("%d ", v[i]);
    }
    printf("\n");
}

static bool is_sorted(int *v, long l)
{
    if (l == 0)
        return true;
    int prev = v[0];
    for (size_t i = 1; i < l; ++i)
        if (prev > v[i])
            return false;
    return true;
}

int main(int argc, char **argv) {

    int c;
    int seed = 42;
    long length = 1e4;
    Order order = ASCENDING;
    struct timeval before, after;
    int *vector;

    /* Read command-line options. */
    while((c = getopt(argc, argv, "adrgtl:s:")) != -1) {
        switch(c) {
            case 'a':
                order = ASCENDING;
                break;
            case 'd':
                order = DESCENDING;
                break;
            case 'r':
                order = RANDOM;
                break;
            case 'l':
                length = atol(optarg);
                break;
            case 'g':
                debug = 1;
                break;
            case 't':
                chrono = 1;
                break;
            case 's':
                seed = atoi(optarg);
                break;
            case '?':
                if(optopt == 'l' || optopt == 's') {
                    fprintf(stderr, "Option -%c requires an argument.\n", optopt);
                }
                else if(isprint(optopt)) {
                    fprintf(stderr, "Unknown option '-%c'.\n", optopt);
                }
                else {
                    fprintf(stderr, "Unknown option character '\\x%x'.\n", optopt);
                }
                return -1;
            default:
                return -1;
        }
    }

    /* Seed such that we can always reproduce the same random vector */
    srand(seed);

    /* Allocate vector. */
    vector = (int*)malloc(length*sizeof(int));
    if(vector == NULL) {
        fprintf(stderr, "Malloc failed...\n");
        return -1;
    }

    /* Fill vector. */
    switch(order){
        case ASCENDING:
            for(long i = 0; i < length; i++) {
                vector[i] = (int)i;
            }
            break;
        case DESCENDING:
            for(long i = 0; i < length; i++) {
                vector[i] = (int)(length - i);
            } 
            break;
        case RANDOM:
            for(long i = 0; i < length; i++) {
                vector[i] = rand();
            }
            break;
    }

    if(debug)
        print_v(vector, length);

    gettimeofday(&before, NULL);
    msort(vector, length);
    gettimeofday(&after, NULL);

    if (chrono)
        printf("%f\n", (double)(after.tv_sec - before.tv_sec) + 
                (double)(after.tv_usec - before.tv_usec) / 1e6);

    if(debug)
        print_v(vector, length);

    return !is_sorted(vector, length);
}
