#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>

pid_t pid;
char *script;

void my_sigchld(int sig)
{
    char buf[255];
    int status;

    signal(SIGCHLD, SIG_DFL);
    snprintf(buf, sizeof(buf), "%s %d", script, pid);
    system(buf);
    waitpid(pid, &status, 0);
    exit(0);
}

int main(int argc, char **argv)
{
    script = argv[1];
    signal(SIGCHLD, my_sigchld);
    pid = fork();

    if (pid == 0)
        execv(argv[2], &argv[2]);
    else
        while (1)
            sleep(10);
    return 0;
}
