#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <getopt.h>
#include <stdio.h>
#include <ctype.h>
#include <sys/time.h>
#include <omp.h>

typedef enum Ordering {ASCENDING, DESCENDING, RANDOM} Order;

static int debug = 0;
static int chrono = 0;

static void merge(int a[], size_t start, size_t mid, size_t end, int b[])
{
    size_t i0 = start;
    size_t i1 = mid;

    for (size_t j = start; j < end; j++) {
        if (i0 < mid && (i1 >= end || a[i0] <= a[i1])) {
            b[j] = a[i0];
            i0 = i0 + 1;
        }
        else {
            b[j] = a[i1];
            i1 = i1 + 1;
        }
    }
}

static void merge_sort(int a[], size_t start, size_t end, int b[])
{
    if(end - start < 2)
        return;

    size_t i;
    size_t mid = start + (end - start) / 2;

    //#pragma omp task shared(a, b) firstprivate (start, mid, end)
    merge_sort(a, start,  mid, b);
    //#pragma omp task shared(a, b) firstprivate (start, mid, end)
    merge_sort(a, mid, end, b);
    //#pragma omp taskwait
    merge(a, start, mid, end, b);

    /* Copy array */
    //#pragma omp parallel for shared (a, b) private(i)
    for(i = start; i < end; ++i)
        a[i] = b[i];
}

static void vector_sort(int **vector, size_t *len, int n)
{
    #pragma omp parallel for schedule (runtime)
    for (int i = 0; i < n; ++i) {
        int *tmp = malloc(len[i] * sizeof(int));
        merge_sort(vector[i], 0, len[i], tmp);
        free(tmp);
    }
}

static bool is_sorted(int *v, long l)
{
    if (l == 0)
        return true;
    int prev = v[0];
    for (size_t i = 1; i < l; ++i)
        if (prev > v[i])
            return false;
    return true;
}

static bool assert_sorted(int **v, size_t *len, int n)
{
    for (int i = 0; i < n; ++i)
        if (!is_sorted(v[i], len[i]))
            return false;
    return true;
}

int main(int argc, char **argv)
{
    int c;
    int n = 1;
    int chunk = 0;
    int seed = 0x42;
    size_t max = 10000;
    size_t *len;
    int **vector;
    Order order = ASCENDING;
    struct timeval before, after;
    omp_sched_t sched = omp_sched_auto;

    while((c = getopt(argc, argv, "adrgtl:s:n:o:c:")) != -1) {
        switch(c) {
            case 'a':
                order = ASCENDING;
                break;
            case 'd':
                order = DESCENDING;
                break;
            case 'r':
                order = RANDOM;
                break;
            case 'l':
                max = atol(optarg);
                break;
            case 'n':
                n = atol(optarg);
                break;
            case 'c':
                chunk = atol(optarg);
                break;
            case 'g':
                debug = 1;
                break;
            case 't':
                chrono = 1;
                break;
            case 's':
                seed = atoi(optarg);
                break;
            case 'o':
                switch(optarg[0]) {
                    case 's':
                        sched = omp_sched_static;
                        break;
                    case 'd':
                        sched = omp_sched_dynamic;
                        break;
                    case 'g':
                        sched = omp_sched_guided;
                        break;
                    case 'a':
                        sched = omp_sched_auto;
                        break;
                    default:
                        fprintf(stderr, "Option -t <sdga>\n");
                        return -1;
                }
                break;
            case '?':
                if (optopt == 'l' || optopt == 's' || optopt == 'm'
                   || optopt == 't' || optopt == 'c')
                    fprintf(stderr, "Option -%c requires an argument.\n", optopt);
                else if(isprint(optopt))
                    fprintf(stderr, "Unknown option '-%c'.\n", optopt);
                else
                    fprintf(stderr, "Unknown option character '\\x%x'.\n", optopt);
                return -1;
            default:
                return -1;
        }
    }

    srand(seed);
    len = malloc(sizeof(size_t) * n);
    vector = malloc(sizeof(int*) * n);

    for (int i = 0; i < n; ++i) {
        len[i] = rand() % max;
        vector[i] = malloc(sizeof(int) * len[i]);
        switch(order) {
            case ASCENDING:
                for(long j = 0; j < len[i]; j++)
                    vector[i][j] = (int)j;
                break;
            case DESCENDING:
                for(long j = 0; j < len[i]; j++)
                    vector[i][j] = (int)(len[i] - i);
                break;
            case RANDOM:
                for(long j = 0; j < len[i]; j++)
                    vector[i][j] = rand();
                break;
            default:
                return -1;
        }
    }

    omp_set_schedule(sched, chunk);
    gettimeofday(&before, NULL);
    vector_sort(vector, len, n);
    gettimeofday(&after, NULL);

    if (chrono)
        printf("%f\n", (double)(after.tv_sec - before.tv_sec) + 
                (double)(after.tv_usec - before.tv_usec) / 1e6);

    if (!assert_sorted(vector, len, n))
        printf("Wrong results\n");

    return 0;
}
