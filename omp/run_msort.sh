#! /bin/bash

SEED=42
n=1000
#MAX=100000000
MAX=100000000000
#ORDER=(a d r)
ORDER=(a)

make clean > /dev/null
make CFLAGS="-std=gnu99 -O3 -march=native -msse -msse2 -msse3 -msse4.2 -mfpmath=sse" > /dev/null 2>&1
mv mergesort mergesort_single
make clean > /dev/null
make CFLAGS="-std=gnu99 -O3 -march=native -msse -msse2 -msse3 -msse4.2 -mfpmath=sse -fopenmp" > /dev/null 2>&1

printf "          %-15s | Parallel         Single\n" Length
while [ $n -le $MAX ]; do
    for o in ${ORDER[@]}; do
        T1=`prun -v -np 1 ./mergesort_single -$o -l $n -s $SEED -t 2>/dev/null`
        T2=`prun -v -np 1 ./mergesort -$o -l $n -s $SEED -t 2>/dev/null`
        if [ $? -eq 0 ]; then
            echo -n [ OK ]
        else
            echo -n [FAIL]
            exit 0
        fi
        printf " $o: %-15s | $T1 - $T2\n" "$n"
    done
    n=$((n * 3))
done
