#! /bin/sh

n=1000
#MAX=$(cat /proc/sys/kernel/threads-max)
MAX=16000
l=$1

while [ $n -le $MAX ]; do
    echo "n: $n  l: $l"
    prun -v -np 4 bash -c "ulimit -u 200000; ./psort -n $n -l $l" 2>/dev/null | grep Sorted | cut -d " " -f5
    echo
    n=$((n + 2000))
done
