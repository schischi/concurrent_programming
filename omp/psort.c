#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>
#include <errno.h>
#include <string.h>
#include <pthread.h>
#include <sys/time.h>

#define swap(x, y)                           \
    do {                                     \
        typeof(x) tmp = x; x = y; y = tmp;   \
    } while (0)

static inline void *xmalloc(size_t size)
{
    void *ret = malloc(size);
    if (ret == NULL)
        exit(ENOMEM);
    return ret;
}

struct prog_param {
    int running_thread;
    size_t buf_size;
    size_t cnt;
    enum {
        ASCENDING,
        DESCENDING
    } order;
    enum {
        LOG_BASIC  = 0,
        LOG_OUTPUT = 1,
        LOG_IO     = 2,
    } verbosity;
};
#define PROG_PARAM_DEFAULT { \
    .running_thread = 0,     \
    .buf_size = 10,          \
    .cnt = 10,               \
    .order = ASCENDING,      \
    .verbosity = LOG_BASIC   \
}

/* The generator thread produces tokens.
 * A token hold its type and may contain a number */
struct token {
    int n;
    enum {
        T_NUM,
        T_UNDEF,
        T_END,
    } type;
};

/* A latch is the structure used for communications between 2 stages.
 * One stage produce a token (producer) and the other one consume the token
 * (consumer)
 * The synchronization is made using mutex and signals */
struct latch {
    /* Synchronization */
    pthread_mutex_t mutex;
    pthread_cond_t consumed;
    pthread_cond_t produced;
    /* Token array: implemented as a circular buffer (FIFO) */
    size_t start;
    size_t end;
    size_t buf_size;
    struct token tok[];
};

/* Create a latch and initialize its member to their default value */
static struct latch *latch_create(size_t buf_size)
{
    struct latch *l = xmalloc(sizeof(struct latch)
                            + sizeof(struct token) * buf_size);
    l->start = 0;
    l->end = 0;
    l->buf_size = buf_size;
    pthread_mutex_init(&l->mutex, NULL);
    pthread_cond_init(&l->consumed, NULL);
    pthread_cond_init(&l->produced, NULL);
    return l;
}

/* Block until a token is made available by the corresponding producer and
 * return it */
static struct token latch_consume(struct latch *l)
{
    struct token ret;
    pthread_mutex_lock(&l->mutex);

    while (l->start == l->end) //buffer is empty
        pthread_cond_wait(&l->produced, &l->mutex);

    ret = l->tok[l->start];
    l->start = (l->start + 1) % l->buf_size;
    pthread_cond_signal(&l->consumed);
    pthread_mutex_unlock(&l->mutex);

    return ret;
}

/* Block until the previous token has been consumed by the associated
 * consumer and then push the new token */
static void latch_produce(struct latch *l, struct token tok)
{
    pthread_mutex_lock(&l->mutex);

    while ((l->end + 1) % l->buf_size == l->start) //buffer is full
        pthread_cond_wait(&l->consumed, &l->mutex);

    l->tok[l->end] = tok;
    l->end = (l->end + 1) % l->buf_size;
    pthread_cond_signal(&l->produced);
    pthread_mutex_unlock(&l->mutex);
}

/* The description of a pipeline stage. */
struct stage {
    /* Current token held by the stage*/
    struct token tok;
    /* Latch used to communicate with the producer */
    struct latch *latch;
    /* A pointer to the next stage. Used to communicate with the consumer */
    struct stage *next_stage;
    /* The currently running thread */
    pthread_t thread;
    /* Pointer to the global configuration */
    struct prog_param *param;
};

/* Allocate a stage and initialize its member */
static struct stage *stage_create(void *(*fct)(void*), struct prog_param *p)
{
    int err;
    struct stage *s = xmalloc(sizeof(struct stage));

    s->latch = latch_create(p->buf_size);
    s->tok.type = T_UNDEF;
    s->next_stage = NULL;
    s->param = p;

    /* Only one thread can be created at once. Indeed the next thread will
     * be created by the thread we are creating now.
     * Locking the shared variable 'running_thread' is thus not necessary */
    s->param->running_thread += 1;
    if ((err = pthread_create(&s->thread, NULL, fct, s))) {
        fprintf(stderr, "Error creating thread (%s)\n", strerror(err));
        exit(err);
    }

    return s;
}

static void stage_free(struct stage *s)
{
    free(s->latch);
    free(s);
}

static void *stage_print(void *data);

/* Comparator stage of the pipeline */
static void *stage_comparator(void *data)
{
    int err;
    void *ret = NULL;
    struct stage *s = (struct stage*)data;

    while (1) {
        struct token tok = latch_consume(s->latch);
        /* The comparator does not hold a number yet */
        if (s->tok.type == T_UNDEF)
            s->tok = tok;

        /* Comparator */
        else if (tok.type == T_NUM && s->tok.type == T_NUM) {
            if ((s->param->order == ASCENDING && tok.n > s->tok.n)
                || (s->param->order == DESCENDING && tok.n < s->tok.n))
                swap(s->tok, tok);
            if (s->next_stage == NULL) {
                if (s->param->running_thread >= s->param->cnt)
                    s->next_stage = stage_create(stage_print, s->param);
                else
                    s->next_stage = stage_create(stage_comparator, s->param);
            }
            /* Send the data to the next stage */
            latch_produce(s->next_stage->latch, tok);
        }

        /* The end! */
        else if (tok.type == T_END) {
            if (s->next_stage == NULL)
                s->next_stage = stage_create(stage_print, s->param);
            /* Forward the first END */
            latch_produce(s->next_stage->latch, tok);
            /* Send stored token */
            latch_produce(s->next_stage->latch, s->tok);
            do {
                tok = latch_consume(s->latch);
                /* Just forward the tokens */
                latch_produce(s->next_stage->latch, tok);
            } while (tok.type != T_END);
            /* Forward the second END and terminate the thread */
            latch_produce(s->next_stage->latch, tok);
            /* Terminate the next stage first */
            if (s->next_stage) {
                if ((err = pthread_join(s->next_stage->thread, &ret))) {
                    fprintf(stderr, "Error joining thread (%s)\n", strerror(err));
                    exit(err);
                }
                stage_free(s->next_stage);
            }
            return ret;
        }

        else {
            fprintf(stderr, "Undefined behavior, please fix your code.\n");
            exit(1);
        }
    }
}

/* First stage of the pipeline: generate random numbers */
static void *stage_gen_rnd(void *data)
{
    int err;
    void *ret = NULL;
    struct stage *s = NULL;
    struct prog_param *p = (struct prog_param*)data;
    struct token tok = {
        .type = T_NUM
    };

    if (p->verbosity >= LOG_IO)
        printf("INPUT:\n[ ");

    /* Generate 'len' number of random number */
    for (size_t i = 0; i < p->cnt; ++i) {
        tok.n = rand();
        if (p->verbosity >= LOG_IO)
            printf("%d, ", tok.n);
        /* Create the first stage if it does not exist yet */
        if (s == NULL)
            s = stage_create(stage_comparator, p);
        /* Send the produced data to the first stage */
        latch_produce(s->latch, tok);
    }

    if (p->verbosity >= LOG_IO)
        printf("]\n");

    /* No more numbers to be generated, send 2 END token to terminate the
     * threads*/
    tok.type = T_END;
    tok.n = 0;
    latch_produce(s->latch, tok);
    latch_produce(s->latch, tok);

    /* Terminate the first stage */
    if (s) {
        if ((err = pthread_join(s->thread, &ret))) {
            fprintf(stderr, "Error joining thread (%s)\n", strerror(err));
            exit(err);
        }
        stage_free(s);
    }
    return ret;
}

/* Results computed by the pipeline */
struct result {
    int is_sorted;
};

/* Last stage of the pipeline: print the numbers produced by the previous
 * stages. We also check if the numbers are actually sorted */
static void *stage_print(void *data)
{
    int end_cnt = 0;
    struct token prev_tok = {
        .type = T_UNDEF,
    };
    struct stage *s = (struct stage*)data;
    struct result *res = xmalloc(sizeof(struct result));

    /* The output is assumed sorted until proven otherwise */
    res->is_sorted = 1;

    if (s->param->verbosity >= LOG_OUTPUT)
        printf("OUTPUT:\n[ ");

    while (end_cnt != 2) {
        struct token tok = latch_consume(s->latch);
        if (tok.type == T_NUM) {
            /* First token, just store it for the next comparison */
            if (prev_tok.type == T_UNDEF)
                prev_tok = tok;
            /* Check the order of the received token */
            else
                res->is_sorted &= ((s->param->order == ASCENDING
                                    && tok.n > prev_tok.n)
                                || (s->param->order == DESCENDING
                                    && tok.n < prev_tok.n));
            if (s->param->verbosity >= LOG_OUTPUT)
                printf("%d, ", tok.n);
        }
        else if (tok.type == T_END)
            ++end_cnt;
    }
    if (s->param->verbosity >= LOG_OUTPUT)
        printf("]\n");

    return res;
}

static void usage(int argc, char **argv)
{
    printf("usage: %s [options]\n"
           "\n"
           "Options:\n"
           "    -n <length> : number of number to be generated\n"
           "    -o <a|d>    : ascending or  descending sort\n"
           "    -l <size>   : size of the buffer (producer/consumer)\n"
           "    -v <0-2>    : verbosity level\n"
           "    -s <seed>   : srand seed\n"
           , argv[0]
    );
}

int main(int argc, char **argv)
{
    int c;
    unsigned int seed = 0x42;
    struct timeval start, end;
    struct prog_param p = PROG_PARAM_DEFAULT;

    while ((c = getopt(argc, argv, "n:s:o:l:v:h")) != -1) {
        switch (c) {
            case 'n':
                p.cnt = atoi(optarg);
                if (p.cnt == 0)
                    exit(1);
                break;
            case 's':
                seed = atoi(optarg);
                break;
            case 'v':
                p.verbosity = atoi(optarg);
                if (p.verbosity < LOG_BASIC || p.verbosity > LOG_IO)
                    exit(1);
                break;
            case 'o':
                switch (optarg[0]) {
                    case 'a':
                        p.order = ASCENDING;
                        break;
                    case 'd':
                        p.order = DESCENDING;
                        break;
                    default:
                        usage(argc, argv);
                        return 1;
                }
                break;
            case 'l':
                p.buf_size = atoi(optarg);
                if (p.buf_size == 0)
                    exit(1);
                p.buf_size += 1;
                break;
            case 'h':
                usage(argc, argv);
                return 0;
            default:
                usage(argc, argv);
                return 1;
        }
    }
    srand(seed);

    /* Main thread */
    gettimeofday(&start, NULL);
    struct result *res = stage_gen_rnd(&p);
    gettimeofday(&end, NULL);

    if (res->is_sorted)
        printf("Sorted %zu elements in %f seconds.\n",
                p.cnt,
                (double)(end.tv_sec - start.tv_sec)
                + (double)(end.tv_usec - start.tv_usec) / 1e6);
    else
        printf("The output is not sorted. Go fix your code.\n");

    free(res);
    return 0;
}
